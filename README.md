# LOAM

This package was specially made to be used with jackal in a gazebo environment, it allows the user to chose between the lego_loam alogorithm or the aloam alogorithm to draw a map and localize the robot in it, it also records the necessary topics that can be later used with the matlab function available in this repository.  



## Dependency

- [ROS](http://wiki.ros.org/ROS/Installation) (tested with noetic)
- [gtsam](https://github.com/borglab/gtsam/releases) (Georgia Tech Smoothing and Mapping library, 4.0.0-alpha2)
  ```
  wget -O ~/Downloads/gtsam.zip https://github.com/borglab/gtsam/archive/4.0.0-alpha2.zip
  cd ~/Downloads/ && unzip gtsam.zip -d ~/Downloads/
  cd ~/Downloads/gtsam-4.0.0-alpha2/
  mkdir build && cd build
  cmake ..
  sudo make install
  ```

## Compile

You can use the following commands to download and compile the package.

```
cd ~/catkin_ws/src
git clone 
cd ..
catkin_make -j1
```

When you compile the code for the first time, you need to add "-j1" behind "catkin_make" for generating some message types. "-j1" is not needed for future compiling.



## About Lego_loam

LeGO-LOAM is speficifally optimized for a horizontally placed VLP-16 on a ground vehicle. It assumes there is always a ground plane in the scan. The UGV we are using is Clearpath Jackal. It has a built-in IMU. 

The package performs segmentation before feature extraction.

Lidar odometry performs two-step Levenberg Marquardt optimization to get 6D transformation.



## About aloam

A-LOAM is an Advanced implementation of LOAM (J. Zhang and S. Singh. LOAM: Lidar Odometry and Mapping in Real-time), which uses Eigen and Ceres Solver to simplify code structure. This code is modified from LOAM and LOAM_NOTED. This code is clean and simple without complicated mathematical derivation and redundant operations. It is a good learning material for SLAM beginners.



## Run the package

This package was made so that you can easily switch between the lego_loam and aloam algorithms using one command line

1. To run the lego_loam :

```
roslaunch loam lego_loam.launch
```

2. To run the aloam :

```
roslaunch loam aloam.launch
```


3. Play existing bag files :

(All the bags are saved in loam/bags)

to play them again : 

```
roscd loam/bags
```

If you want to play a lego_loam bag :

```
rosbag play lego_bag*.bag
```

Once the bag started playing, run the loam algorithm using the following command : 

```
roslaunch lego_loam run.launch
```



If you want to play an aloam bag :

```
rosbag play aloam_bag*.bag
```

Once the bag started playing, run the loam algorithm using the following command : 

```
roslaunch aloam run.launch
```


The * represents the date and time the bag was recorded